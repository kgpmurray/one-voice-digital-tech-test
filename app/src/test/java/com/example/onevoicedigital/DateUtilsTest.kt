package com.example.onevoicedigital

import com.example.onevoicedigital.utils.getDateFormatted
import junit.framework.Assert.assertEquals
import org.junit.Test
import java.util.*


class DateUtilsTest {

    @Test
    fun `getDateFormatted returns correctly formatted date`() {
        val date = Calendar.Builder().setDate(2014, 11, 9).setTimeZone(
            TimeZone.getTimeZone("UTC")).build().time

        val result = date.getDateFormatted()
        assertEquals("Tue 9th December 2014", result)
    }

}