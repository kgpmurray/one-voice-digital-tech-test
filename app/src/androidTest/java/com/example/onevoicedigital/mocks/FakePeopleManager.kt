package com.example.onevoicedigital.mocks

import com.example.onevoicedigital.data.models.people.PeopleResponseWrapper
import com.example.onevoicedigital.data.models.remote.PeopleManager
import com.example.onevoicedigital.data.models.remote.PeopleService
import io.reactivex.rxjava3.core.Single

class FakePeopleManager(private val hasNetwork: Boolean = true, peopleService: PeopleService): PeopleManager(peopleService) {

    override fun getAllPeople(pageNumber: Int): Single<PeopleResponseWrapper> {
        if(hasNetwork){
            return Single.just(mockPeopleResponse)
        } else {
            return Single.error(Throwable())
        }
    }
}