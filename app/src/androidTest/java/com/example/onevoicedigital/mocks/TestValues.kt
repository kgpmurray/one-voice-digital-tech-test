package com.example.onevoicedigital.mocks

import com.example.onevoicedigital.data.models.people.PeopleResponseWrapper
import com.example.onevoicedigital.data.models.people.Person
import java.util.*

val mockPeople: List<Person> = listOf(
    Person(name = "Luke Skywalker", height = "172", mass = "77",
    createdDate = Calendar.Builder().setTimeZone(TimeZone.getTimeZone("GMT")).setDate(2014, 12, 9).build().time),
    Person(name = "C-3PO", height = "167", mass = "75",
        createdDate = Calendar.Builder().setTimeZone(TimeZone.getTimeZone("GMT")).setDate(2014, 12, 10).build().time))
val mockPeopleResponse: PeopleResponseWrapper = PeopleResponseWrapper(count = 1, results = mockPeople)