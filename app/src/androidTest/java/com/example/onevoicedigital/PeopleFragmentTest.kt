package com.example.onevoicedigital

import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.onevoicedigital.data.models.remote.PeopleManager
import com.example.onevoicedigital.data.models.remote.PeopleModule
import com.example.onevoicedigital.data.models.remote.PeopleService
import com.example.onevoicedigital.mocks.FakePeopleManager
import com.example.onevoicedigital.ui.MainActivity
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import org.hamcrest.CoreMatchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import retrofit2.Retrofit

//ToDo with additional time test views display correctly test retry button shows when request fails etc
//This test is mainly here to show how I might implement future tests

@UninstallModules(PeopleModule::class)
@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
class PeopleFragmentTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @Module
    @InstallIn(ActivityRetainedComponent::class)
    object FakePeopleModule {

        @Provides
        fun providePeopleManager(peopleService: PeopleService): PeopleManager {
            return FakePeopleManager(true, peopleService)
        }

        @Provides
        fun providePeopleService(retrofit: Retrofit): PeopleService {
            return retrofit.create(PeopleService::class.java)
        }
    }

    @Test
    fun testRetryButtonNotVisibleWhenTheApiReturnsResults() {
        launchActivity<MainActivity>()
        onView(withId(R.id.retryButton)).check(matches(not(isDisplayed())))
    }
}