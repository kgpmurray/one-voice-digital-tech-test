package com.example.onevoicedigital.utils

/**
 * originally from - https://gist.github.com/ssinss/e06f12ef66c51252563e
 */
abstract class EndlessRecyclerOnScrollListener(private val mLinearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager) : androidx.recyclerview.widget.RecyclerView.OnScrollListener() {

    private var previousTotal = 0 // The total number of items in the dataset after the last load
    private var loading = true // True if we are still waiting for the last set of data to load.
    private val visibleThreshold = 5 // The minimum amount of items to have below your current scroll position before loading more.
    private var firstVisibleItem: Int = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var hasHeaderView: Boolean = false

    private var currentPage = 1

    override fun onScrolled(recyclerView: androidx.recyclerview.widget.RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        visibleItemCount = recyclerView.childCount
        totalItemCount = mLinearLayoutManager.itemCount

        firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition()
        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false
                previousTotal = totalItemCount
            }
        }
        if (!loading && totalItemCount - visibleItemCount <= firstVisibleItem + visibleThreshold) {
            // End has been reached
            currentPage++
            onLoadMore(currentPage)

            loading = true
        }
    }

    fun reset() {
        previousTotal = if (hasHeaderView) 1 else 0
        currentPage = 1
        loading = true
    }

    //Account for the header view
    fun hasHeaderView(hasHeaderView: Boolean) {
        this.hasHeaderView = hasHeaderView
        if (previousTotal == 0) {
            previousTotal = 1
        }
    }

    abstract fun onLoadMore(currentPage: Int)

    companion object {
        var TAG = EndlessRecyclerOnScrollListener::class.java.simpleName
    }
}