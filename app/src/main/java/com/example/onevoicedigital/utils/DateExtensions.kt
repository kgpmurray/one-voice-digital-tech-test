package com.example.onevoicedigital.utils

import java.text.SimpleDateFormat
import java.util.*

fun Date.getDateFormatted(): String{
    val monthSuffix = getDayOfMonthSuffix(Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
    return SimpleDateFormat("EEE d'$monthSuffix' MMMM yyyy", Locale.getDefault()).format(this)
}

fun getDayOfMonthSuffix(n: Int): String =
    when {
        n in 11..13 -> "th"
        n % 10 == 1 -> "st"
        n % 10 == 2 -> "nd"
        n % 10 == 3 -> "rd"
        else -> "th"
    }

