package com.example.onevoicedigital.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.onevoicedigital.R
import com.example.onevoicedigital.ui.people.PeopleFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .add(R.id.container, PeopleFragment.newInstance())
                    .commitNow()
        }
    }
}