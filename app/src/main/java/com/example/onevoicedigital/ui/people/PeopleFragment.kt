package com.example.onevoicedigital.ui.people

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.onevoicedigital.R
import com.example.onevoicedigital.people.PeopleAdapter
import com.example.onevoicedigital.utils.EndlessRecyclerOnScrollListener
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.people_fragment.*

//ToDo add no more items indicator to end of list / prevent extra api requests
//ToDo pull to refresh to clear list and reload
@AndroidEntryPoint
class PeopleFragment : Fragment() {

    companion object {
        fun newInstance() = PeopleFragment()
    }

    private val viewModel: PeopleViewModel by viewModels()
    private val peopleAdapter: PeopleAdapter = PeopleAdapter()

    private val endlessScrollListener: EndlessRecyclerOnScrollListener by lazy {
        object : EndlessRecyclerOnScrollListener(peopleRecyclerView.layoutManager as androidx.recyclerview.widget.LinearLayoutManager) {
            override fun onLoadMore(currentPage: Int) {
                viewModel.fetchNextPage()
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.people_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupAdapter()

        //Observe livedata
        viewModel.peopleLiveData.observe(viewLifecycleOwner, {
            peopleAdapter.setItems(it)
        })

        viewModel.viewState.observe(viewLifecycleOwner, {
            if(it.loading) {
                peopleProgressBar.visibility = View.VISIBLE
            } else {
                peopleProgressBar.visibility = View.GONE
            }

            when(it.error){
                PeopleViewModel.ErrorType.GENERIC_ERROR -> {
                    Toast.makeText(requireContext(), getString(R.string.generic_error), Toast.LENGTH_SHORT).show()
                    retryButton.visibility = View.VISIBLE
                }
            }
        })

        retryButton.setOnClickListener {
            it.visibility = View.GONE
            viewModel.fetchNextPage()
        }
    }

    private fun setupAdapter(){
        val dividerItemDecoration = DividerItemDecoration(peopleRecyclerView.context,
            ((peopleRecyclerView.layoutManager) as LinearLayoutManager).orientation)
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(peopleRecyclerView.context, R.drawable.white_line_shape)!!)
        peopleRecyclerView.addItemDecoration(dividerItemDecoration)
        peopleRecyclerView.addOnScrollListener(endlessScrollListener)
        peopleRecyclerView.adapter = peopleAdapter

        peopleAdapter.personClickedListener = {
            requireActivity().supportFragmentManager.beginTransaction()
                .add(R.id.container, PersonDetailsFragment.newInstance(it))
                .addToBackStack("PersonDetailsFragment")
                .commit()
        }
    }


}