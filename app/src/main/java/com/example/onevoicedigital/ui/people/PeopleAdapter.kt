package com.example.onevoicedigital.people

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.onevoicedigital.R
import com.example.onevoicedigital.data.models.people.Person
import kotlinx.android.synthetic.main.people_list_item.view.*

class PeopleAdapter : RecyclerView.Adapter<PeopleAdapter.PeopleItemViewHolder>() {

    private var items: MutableList<Person> = mutableListOf()

    var personClickedListener: ((person: Person) -> Unit)? = null

    fun clearList(){
        items = mutableListOf()
        notifyDataSetChanged()
    }

    fun setItems(newItems: List<Person>) {
        val tempItems: MutableList<Person> = mutableListOf()
        tempItems.addAll(newItems)
        val callback = PeopleDiffCallback(items, tempItems)

        items = tempItems
        DiffUtil.calculateDiff(callback, false).dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PeopleItemViewHolder =
        PeopleItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.people_list_item, parent, false))

    override fun onBindViewHolder(holder: PeopleItemViewHolder, position: Int) {
        val person = items[position]
        holder.itemView.apply {
            personName.text = person.name

            setOnClickListener {
                personClickedListener?.invoke(person)
            }
        }
    }

    override fun getItemCount(): Int = items.size

    class PeopleItemViewHolder(view: View) : RecyclerView.ViewHolder(view)



}

class PeopleDiffCallback(private val oldList: List<Person>,
                         private val newList: List<Person>) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldList[oldItemPosition] == newList[newItemPosition]

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldList[oldItemPosition] == newList[newItemPosition]

}

