package com.example.onevoicedigital.ui.people

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.onevoicedigital.data.models.people.Person
import com.example.onevoicedigital.data.models.remote.PeopleManager

class PeopleViewModel @ViewModelInject constructor(private val peopleManager: PeopleManager) : ViewModel() {
    private var currentPage = 1
    val viewState : MutableLiveData<PeopleViewState> by lazy {
        MutableLiveData(PeopleViewState())
    }
    val peopleLiveData : MutableLiveData<MutableList<Person>> = MutableLiveData(mutableListOf())

    init {
        fetchNextPage()
    }

    fun fetchNextPage(){
        viewState.postValue(viewState.value?.update(loading = true))
        peopleManager.getAllPeople(currentPage)
            .subscribe ({
                val tempList = peopleLiveData.value
                tempList?.addAll(it.results)
                peopleLiveData.postValue(tempList)
                currentPage++
                viewState.postValue(viewState.value?.update(loading = false, moreItemsToLoad = ((it.count / 10) > currentPage-1)))
            },{
                it.printStackTrace()
                viewState.postValue(viewState.value?.update(loading = false, error = ErrorType.GENERIC_ERROR))
            })
    }

    data class PeopleViewState(
        val loading: Boolean = false,
        val moreItemsToLoad: Boolean = false,
        val error: ErrorType? = null){

        fun update(loading: Boolean? = null, moreItemsToLoad: Boolean? = null, error: ErrorType? = null) : PeopleViewState {
            return PeopleViewState(loading ?: this.loading,
                moreItemsToLoad ?: this.moreItemsToLoad, error)
        }
    }

    //ToDo implement more detailed error messages
    enum class ErrorType {
        GENERIC_ERROR
    }
}