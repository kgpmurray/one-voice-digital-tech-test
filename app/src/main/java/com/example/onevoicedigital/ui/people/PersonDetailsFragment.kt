package com.example.onevoicedigital.ui.people

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.onevoicedigital.R
import com.example.onevoicedigital.data.models.people.Person
import com.example.onevoicedigital.utils.getDateFormatted
import kotlinx.android.synthetic.main.people_list_item.personName
import kotlinx.android.synthetic.main.person_details_fragment.*

//ToDo requires viewmodel to follow MVVM but probably to simple to need it
class PersonDetailsFragment : Fragment() {

    val person: Person? by lazy {
        requireArguments().getParcelable(PERSON)
    }

    companion object {
        const val PERSON = "PERSON"
        fun newInstance(person: Person): PersonDetailsFragment = PersonDetailsFragment().apply {
            arguments = Bundle().apply {
                putParcelable(PERSON, person)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.person_details_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        personName.text = person?.name
        personHeight.text = getString(R.string.height_cm, person?.height)
        personMass.text = getString(R.string.mass_kg, person?.mass)
        personCreatedAt.text = person?.createdDate?.getDateFormatted()
    }

}