package com.example.onevoicedigital.data.models.remote

import com.example.onevoicedigital.data.models.people.PeopleResponseWrapper
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query


interface PeopleService {
    @GET("people")
    fun getPeople(@Query("page") pageNumber: Int): Single<PeopleResponseWrapper>
}


