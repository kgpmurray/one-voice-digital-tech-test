package com.example.onevoicedigital.data.models.remote

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import retrofit2.Retrofit

@Module
@InstallIn(ActivityRetainedComponent::class)
object PeopleModule {
    @Provides
    fun providePeopleService(retrofit: Retrofit): PeopleService {
        return retrofit.create(PeopleService::class.java)
    }

    @Provides
    fun providePeopleManager(peopleService: PeopleService): PeopleManager {
        return PeopleManager(peopleService)
    }
}