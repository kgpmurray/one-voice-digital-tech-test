package com.example.onevoicedigital.data.models.remote

import com.example.onevoicedigital.data.models.people.PeopleResponseWrapper
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

open class PeopleManager @Inject constructor(private val peopleService: PeopleService) {
    open fun getAllPeople(pageNumber: Int): Single<PeopleResponseWrapper> =
        peopleService.getPeople(pageNumber)
            .retry(1)
            .observeOn(AndroidSchedulers.mainThread())
}