package com.example.onevoicedigital.data.models.people

import com.google.gson.annotations.SerializedName

data class PeopleResponseWrapper(
    @SerializedName("count") val count: Int,
    @SerializedName("results") val results: List<Person>
)