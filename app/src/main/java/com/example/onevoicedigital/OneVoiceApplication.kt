package com.example.onevoicedigital

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class OneVoiceApplication : Application() {
}